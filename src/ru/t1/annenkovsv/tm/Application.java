package ru.t1.annenkovsv.tm;

import static ru.t1.annenkovsv.tm.constant.TerminalConstant.*;

public final class Application {

    public static void main(String[] args) {
        processArguments(args);
    }

    public static void processArguments(String[] args) {
        if (args == null || args.length == 0) return;
        final String argument = args[0];
        processArgument(argument);
    }

    public static void processArgument(final String argument) {
        if (argument == null || argument.isEmpty()) return;
        switch (argument.toUpperCase()) {
            case HELP:
                showHelp();
                break;
            case ABOUT:
                showAbout();
                break;
            case VERSION:
                showVersion();
                break;
            default:
                showError();
                break;
        }
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.printf("Developer name: %s\n", DEVELOPERNAME);
        System.out.printf("Email: %s\n", EMAIL);
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.println("Current task manager command list:");
        System.out.printf("%s: Information on developer.\n", ABOUT);
        System.out.printf("%s: Program commands list.\n", HELP);
        System.out.printf("%s: Current task manager version.\n", VERSION);
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.printf("Current program version: %s\n", CURRENTVERSION);
        System.out.printf("Last updated: %s\n", LASTUPDATED);
    }

    public static void showError() {
        System.err.println("[ERROR]");
        System.err.println("Input arguments are not valid, please use commands from list below:");
        System.err.printf("%s, %s, %s", HELP, ABOUT, VERSION);
    }

}