# TASK MANAGER

## DEVELOPER INFO

**Name**: Sergey Annenkov

**E-mail**: glaatum@gmail.com

## SOFTWARE

**OS**: Windows 10 21H1 19043.1706

**JDK**: OPENJDK 1.8.0_275

## HARDWARE

**CPU**: AMD Ryzen 7 2700x 3.70 GHz

**RAM**: HyperX Predator 16GB 3200 MHz

**SSD**: Samsung 860 Evo 512 GB

## RUN PROGRAM

```bash
java -jar ./task-manager.jar help
```
